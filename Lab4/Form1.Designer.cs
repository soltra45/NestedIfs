﻿namespace Lab4
{
    partial class Lab4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.highSchoolGPALbl = new System.Windows.Forms.Label();
            this.admissionTestScoreLbl = new System.Windows.Forms.Label();
            this.highSchoolGPATxt = new System.Windows.Forms.TextBox();
            this.admissionTestScoreTxt = new System.Windows.Forms.TextBox();
            this.calcBtn = new System.Windows.Forms.Button();
            this.outputLbl = new System.Windows.Forms.TextBox();
            this.numberAcceptedLbl = new System.Windows.Forms.Label();
            this.numberRejectedLbl = new System.Windows.Forms.Label();
            this.acceptedCounterLbl = new System.Windows.Forms.Label();
            this.rejectedCounterLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // highSchoolGPALbl
            // 
            this.highSchoolGPALbl.AutoSize = true;
            this.highSchoolGPALbl.Location = new System.Drawing.Point(130, 66);
            this.highSchoolGPALbl.Name = "highSchoolGPALbl";
            this.highSchoolGPALbl.Size = new System.Drawing.Size(137, 20);
            this.highSchoolGPALbl.TabIndex = 0;
            this.highSchoolGPALbl.Text = "High School GPA:";
            // 
            // admissionTestScoreLbl
            // 
            this.admissionTestScoreLbl.AutoSize = true;
            this.admissionTestScoreLbl.Location = new System.Drawing.Point(100, 126);
            this.admissionTestScoreLbl.Name = "admissionTestScoreLbl";
            this.admissionTestScoreLbl.Size = new System.Drawing.Size(167, 20);
            this.admissionTestScoreLbl.TabIndex = 1;
            this.admissionTestScoreLbl.Text = "Admission Test Score:";
            // 
            // highSchoolGPATxt
            // 
            this.highSchoolGPATxt.Location = new System.Drawing.Point(312, 63);
            this.highSchoolGPATxt.Name = "highSchoolGPATxt";
            this.highSchoolGPATxt.Size = new System.Drawing.Size(100, 26);
            this.highSchoolGPATxt.TabIndex = 2;
            // 
            // admissionTestScoreTxt
            // 
            this.admissionTestScoreTxt.Location = new System.Drawing.Point(312, 123);
            this.admissionTestScoreTxt.Name = "admissionTestScoreTxt";
            this.admissionTestScoreTxt.Size = new System.Drawing.Size(100, 26);
            this.admissionTestScoreTxt.TabIndex = 3;
            // 
            // calcBtn
            // 
            this.calcBtn.Location = new System.Drawing.Point(312, 183);
            this.calcBtn.Margin = new System.Windows.Forms.Padding(2);
            this.calcBtn.Name = "calcBtn";
            this.calcBtn.Size = new System.Drawing.Size(99, 34);
            this.calcBtn.TabIndex = 4;
            this.calcBtn.Text = "Enter";
            this.calcBtn.UseVisualStyleBackColor = true;
            this.calcBtn.Click += new System.EventHandler(this.calcBtn_Click);
            // 
            // outputLbl
            // 
            this.outputLbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.outputLbl.Location = new System.Drawing.Point(134, 191);
            this.outputLbl.Name = "outputLbl";
            this.outputLbl.Size = new System.Drawing.Size(82, 26);
            this.outputLbl.TabIndex = 5;
            // 
            // numberAcceptedLbl
            // 
            this.numberAcceptedLbl.AutoSize = true;
            this.numberAcceptedLbl.Location = new System.Drawing.Point(116, 257);
            this.numberAcceptedLbl.Name = "numberAcceptedLbl";
            this.numberAcceptedLbl.Size = new System.Drawing.Size(137, 20);
            this.numberAcceptedLbl.TabIndex = 6;
            this.numberAcceptedLbl.Text = "Number Accepted";
            // 
            // numberRejectedLbl
            // 
            this.numberRejectedLbl.AutoSize = true;
            this.numberRejectedLbl.Location = new System.Drawing.Point(285, 257);
            this.numberRejectedLbl.Name = "numberRejectedLbl";
            this.numberRejectedLbl.Size = new System.Drawing.Size(133, 20);
            this.numberRejectedLbl.TabIndex = 7;
            this.numberRejectedLbl.Text = "Number Rejected";
            // 
            // acceptedCounterLbl
            // 
            this.acceptedCounterLbl.AutoSize = true;
            this.acceptedCounterLbl.Location = new System.Drawing.Point(165, 292);
            this.acceptedCounterLbl.Name = "acceptedCounterLbl";
            this.acceptedCounterLbl.Size = new System.Drawing.Size(0, 20);
            this.acceptedCounterLbl.TabIndex = 8;
            // 
            // rejectedCounterLbl
            // 
            this.rejectedCounterLbl.AutoSize = true;
            this.rejectedCounterLbl.Location = new System.Drawing.Point(345, 292);
            this.rejectedCounterLbl.Name = "rejectedCounterLbl";
            this.rejectedCounterLbl.Size = new System.Drawing.Size(0, 20);
            this.rejectedCounterLbl.TabIndex = 9;
            // 
            // Lab4
            // 
            this.AcceptButton = this.calcBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 358);
            this.Controls.Add(this.rejectedCounterLbl);
            this.Controls.Add(this.acceptedCounterLbl);
            this.Controls.Add(this.numberRejectedLbl);
            this.Controls.Add(this.numberAcceptedLbl);
            this.Controls.Add(this.outputLbl);
            this.Controls.Add(this.calcBtn);
            this.Controls.Add(this.admissionTestScoreTxt);
            this.Controls.Add(this.highSchoolGPATxt);
            this.Controls.Add(this.admissionTestScoreLbl);
            this.Controls.Add(this.highSchoolGPALbl);
            this.Name = "Lab4";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label highSchoolGPALbl;
        private System.Windows.Forms.Label admissionTestScoreLbl;
        private System.Windows.Forms.TextBox highSchoolGPATxt;
        private System.Windows.Forms.TextBox admissionTestScoreTxt;
        private System.Windows.Forms.Button calcBtn;
        private System.Windows.Forms.TextBox outputLbl;
        private System.Windows.Forms.Label numberAcceptedLbl;
        private System.Windows.Forms.Label numberRejectedLbl;
        private System.Windows.Forms.Label acceptedCounterLbl;
        private System.Windows.Forms.Label rejectedCounterLbl;
    }
}

