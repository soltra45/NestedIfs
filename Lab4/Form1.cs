﻿// Creating a form application utilizing decisions.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Lab4 : Form
    {
        //variables
        private double highSchoolGPA = 0;
        private double admissionTestScore = 0;
        private const double GPA_THRESHOLD = 3.0;
        private const double LOWER_SCORE = 60;
        private const double HIGHER_SCORE = 80;

        private int acceptCount = 0;
        private int rejectCount = 0;

        public Lab4()
        {
            InitializeComponent();
        }


        private void calcBtn_Click(object sender, EventArgs e) //Event handler for the enter button
        {
             

            //if statement to check for valid GPA
            if (double.TryParse(highSchoolGPATxt.Text, out highSchoolGPA)) //converts to double if applicable
            {
                //if statement to check for valid admission test score
                if (double.TryParse(admissionTestScoreTxt.Text, out admissionTestScore)) //converts to double if applicable
                    //if statement to check if the student should be accepted or rejected
                    {
                    if ((highSchoolGPA >= GPA_THRESHOLD && admissionTestScore >= LOWER_SCORE) || (highSchoolGPA < GPA_THRESHOLD && admissionTestScore >= HIGHER_SCORE))
                    {
                        outputLbl.Text = "Accept"; //output if student is to be accepted
                        acceptCount++; //adds 1 to the number accepted
                        acceptedCounterLbl.Text = Convert.ToString(acceptCount); //shows current number accepted
                    }
                    else
                    {
                        outputLbl.Text = "Reject"; //output if student is to be rejected
                        rejectCount++; //adds 1 to the number rejected
                        rejectedCounterLbl.Text = Convert.ToString(rejectCount); //shows current number rejected
                    }

                }
                else
                    MessageBox.Show("Enter a valid Admission Test Score"); //shown if test score field is invalid
            }
            else
                MessageBox.Show("Enter a valid GPA"); //shown if GPA field is invalid
            
        }
    }
}
